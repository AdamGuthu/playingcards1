/*
 -Enumeration - finite set of values
   - similar to classes where you can use it as a datatype once created
   - When running a code with enumerations in it, the names will be replaced with integers
   - Structs - how you would group code
   - Can be used in C# 
   - Similar to Classes
   - if you are trying to group data use a struct, if you're trying to store methods use a class


*/

// Lab Exercise 2 Playing Cards //
#include <iostream>
#include <conio.h>

using namespace std;

// set of values
enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, HEART, CLUB, DIAMOND
};

// Stores data
struct Card
{
	Rank rank;
	Suit suit;
};




main()
{

	
	




	_getch();
	return 0;
}

void PrintCard(Card card)
{
      cout << "The ";
	  switch (card.rank)
	  {
	  case TWO: std::cout << "two"; break;
	  case THREE: std::cout << "three"; break;
	  case FOUR: std::cout << "four"; break;
	  case FIVE: std::cout << "five"; break;
	  case SIX: std::cout << "six"; break;
	  case SEVEN: std::cout << "seven"; break;
	  case EIGHT: std::cout << "eight"; break;
	  case NINE: std::cout << "nine"; break;
	  case TEN: std::cout << "ten"; break;
	  case JACK: std::cout << "jack"; break;
	  case QUEEN: std::cout << "queen"; break;
	  case KING: std::cout << "king"; break;
	  case ACE: std::cout << "ace"; break;
	  }
	  cout << "of ";
	  switch (card.suit)
	  {
	  case SPADE: std::cout << "spade \n"; break;
	  case HEART: std::cout << "heart \n"; break;
	  case CLUB: std::cout << "club \n"; break;
	  case DIAMOND: std::cout << "diamond \n"; break;






	  }
	
}

Card HighCard(Card c1, Card c2)
{
	if (c1.rank < c2.rank) return c1;

	return c2;
}